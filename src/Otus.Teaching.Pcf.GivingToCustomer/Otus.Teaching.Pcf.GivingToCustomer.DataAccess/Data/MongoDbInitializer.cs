﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public class MongoDbInitializer
        : IDbInitializer
    {
        private readonly IMongoDatabase _database;

        public MongoDbInitializer(IMongoDatabase database)
        {
            _database = database;
        }
        
        public void InitializeDb()
        {
            _database.GetCollection<Preference>("preferences").InsertMany(FakeDataFactory.Preferences);
            _database.GetCollection<Customer>("customers").InsertMany(FakeDataFactory.Customers);
        }
    }
}