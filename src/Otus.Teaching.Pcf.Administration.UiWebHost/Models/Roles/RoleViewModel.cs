﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.UiWebHost.Models.Roles
{
    public class RoleViewModel
    {
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Должно быть заполнено")]
        [Display(Name="Наименование")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Должно быть заполнено")]
        [Display(Name = "Комментарий")]
        public string Description { get; set; }
    }
}
