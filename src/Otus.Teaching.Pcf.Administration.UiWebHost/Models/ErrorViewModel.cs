using System;

namespace Otus.Teaching.Pcf.Administration.UiWebHost.Models
{
    public class ErrorViewModel
    {
        public string RequestId { get; set; }

        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
}
