﻿using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.UiWebHost.Models.Roles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.UiWebHost.Extensions.Roles
{
    public static class RoleExtensions
    {
        public static RoleViewModel ToViewModel(this Role role)
        {
            return new RoleViewModel()
            {
                Id = role.Id,
                Name = role.Name,
                Description = role.Description
            };
        }
        public static Role ToDomainModel(this RoleViewModel role)
        {
            return new Role()
            {
                Id = role.Id == null ? Guid.NewGuid(): role.Id,
                Name = role.Name,
                Description = role.Description
            };
        }
    }
}
