﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.UiWebHost.Extensions.Roles;
using Otus.Teaching.Pcf.Administration.UiWebHost.Models.Roles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.UiWebHost.Controllers
{
    public class RolesController : Controller
    {
        private readonly IRepository<Role> _rolesRepository;

        public RolesController(IRepository<Role> rolesRepository)
        {
            _rolesRepository = rolesRepository;
        }


        public async Task<IActionResult> Index()
        {
            var roles = await _rolesRepository.GetAllAsync();
            var rolesModel = roles.Select(x => x.ToViewModel()).ToList();

            return View(rolesModel);
        }


        [HttpGet]
        public async Task<IActionResult> Edit([FromRoute]Guid id)
        {
            var role = await _rolesRepository.GetByIdAsync(id);
            RoleViewModel viewModel = role.ToViewModel();
            return View(viewModel);
        }


        [HttpPost]
        public async Task<IActionResult> Edit(RoleViewModel model)
        {
            if (ModelState.IsValid)
            {
                var existingRole = await _rolesRepository.GetByIdAsync(model.Id);
                
                existingRole.Name = model.Name;
                existingRole.Description = model.Description;
                
                await _rolesRepository.UpdateAsync(existingRole);
                
                return RedirectToAction("Index");
            }

            return View(model);

        }


        [HttpGet]
        public async Task<IActionResult> Add()
        {
            RoleViewModel viewModel = new RoleViewModel();
            return View(viewModel);
        }


        [HttpPost]
        public async Task<IActionResult> Add(RoleViewModel model)
        {
            if (ModelState.IsValid)
            {
                var role = model.ToDomainModel();
                await _rolesRepository.AddAsync(role);
                return RedirectToAction("Index");
            }

            return View(model);
            
        }


        [HttpGet]
        public async Task<IActionResult> Remove([FromRoute] Guid id)
        {
            var role = await _rolesRepository.GetByIdAsync(id);
            await _rolesRepository.DeleteAsync(role);
            return RedirectToAction("Index");
        }


        [HttpPost]
        public async Task<string> RemoveJQ( Guid id)
        {
            var role = await _rolesRepository.GetByIdAsync(id);
            await _rolesRepository.DeleteAsync(role);
            return Url.ActionLink("Index");
        }
    }
}
